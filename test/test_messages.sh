#!/bin/bash

for msg in ../*.http
do
  echo -n "${msg}... "
  n=`( cat ${msg}; sleep 2; echo -e "\n\n") | nc -q 0 web1-app.tadsufpr.net.br 80 | grep 'HTTP/1.1 200\|HTTP/1.1 302\|HTTP/1.1 201' | wc -l`

  if [[ ${n} < 1 ]]; then
    echo "FAILED."
  else
    echo "OK!"
  fi
done
