Tecnologia em Análise e Desenvolvimento de Sistemas

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*DS122 - Desenvolvimento de Aplicações Web 1*

Prof. Alexander Robert Kutzke

# Atividade: Mensagens HTTP

Preencha todos os arquivos `*.http` do repositório com as mensagens para acessar todas as requisições descritas abaixo para o site http://200.236.3.126:3000/

Utilize o comando `curl` para definir o conteúdo das mensagens.

O site responde às seguintes requisições:

| Verbo HTTP | Padrão UR   | Função                              |
|-----------:|-------------|-------------------------------------|
|        GET | /posts      | Lista posts (index)                 |
|       POST | /posts      | Cria um novo post (create)          |
|        GET | /posts/:id  | Exibe o post número "id" (show)     |
|        PUT | /posts/:id  | Altera o post número "id" (update)  |
|     DELETE | /posts/:id  | Remove o post número "id" (delete)  |

Siga o arquivo `index` como exemplo.

```bash
curl -v 200.236.3.126:3000/posts   
```
